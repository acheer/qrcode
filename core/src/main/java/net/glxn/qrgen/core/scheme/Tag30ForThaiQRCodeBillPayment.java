package net.glxn.qrgen.core.scheme;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class Tag30ForThaiQRCodeBillPayment {

    Map<String, String> tag30 = new HashMap<>();
    private final String PAYMENTTYPE_TAG_ID = "00";
    private final String BILLER_TAG_ID = "01";
    private final String TICKETNO_TAG_ID = "02";

    public void setValue(String tagID, String value) {
        tag30.put(tagID, value);
    }

    public String generateString() {
        String tag30Inside = PAYMENTTYPE_TAG_ID + countLength(tag30.get(PAYMENTTYPE_TAG_ID)) + tag30.get(PAYMENTTYPE_TAG_ID) +
                BILLER_TAG_ID + countLength(tag30.get(BILLER_TAG_ID)) + tag30.get(BILLER_TAG_ID) +
                TICKETNO_TAG_ID + countLength(tag30.get(TICKETNO_TAG_ID)) + tag30.get(TICKETNO_TAG_ID);
        return countLength(tag30Inside) + tag30Inside;
    }

    public String getPaymentID() {
        return tag30.get(PAYMENTTYPE_TAG_ID);
    }

    public String getBillerID() {
        return tag30.get(BILLER_TAG_ID);
    }

    public String getTicketNo() {
        return tag30.get(TICKETNO_TAG_ID);
    }

    private String countLength(String input) {
        return StringUtils.leftPad("" + input.length(), 2, "0");
    }

}
