package net.glxn.qrgen.core.scheme;

import org.apache.commons.lang3.StringUtils;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class ThaiQRCodeBillPayment extends Schema {

    // Define EMCco QR "000201"
    // Define usage type eg Single use "010212"

    private Map<String, String> mapping = new HashMap<>();

    private final String EMCO_TAG_ID = "00";
    private final String USAGETYPE_TAG_ID = "01";
    private final String CURRENCY_TAG_ID = "53";
    private final String AMOUNT_TAG_ID = "54";
    private final String COUNTRY_TAG_ID = "58";
    private final String TAG30_TAG_ID = "30";
    private final String PAYMENTTYPE_TAG_ID = "00";
    private final String BILLER_TAG_ID = "01";
    private final String TICKETNO_TAG_ID = "02";
    private final String REF_TAG_ID = "62";
    private final String CHECKSUM_TAG_ID = "64";
    private Tag30ForThaiQRCodeBillPayment tag30ForThaiQRCodeBillPayment;

    public ThaiQRCodeBillPayment() {
        this.tag30ForThaiQRCodeBillPayment = new Tag30ForThaiQRCodeBillPayment();
    }

    // Ref 03
    private final String ref = "200716SCOSM80012909991";

    public String getEmvcoQR() {
        return this.mapping.get(EMCO_TAG_ID);
    }

    public void setEmvcoQR(String emvcoQR) {
        mapping.put(EMCO_TAG_ID, emvcoQR);
    }

    public String getUseType() {
        return this.mapping.get(USAGETYPE_TAG_ID);
    }

    public void setUseType(String useType) {
        mapping.put(USAGETYPE_TAG_ID, useType);
    }

    public String getCurrency() {
        return this.mapping.get(CURRENCY_TAG_ID);
    }

    public void setCurrency(String currency) {
        this.mapping.put(CURRENCY_TAG_ID, currency);
    }

    public String getAmount() {
        return this.mapping.get(AMOUNT_TAG_ID);
    }

    public void setAmount(double amount) {
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        String numberAsString = decimalFormat.format(amount);
        this.mapping.put(AMOUNT_TAG_ID, numberAsString);
    }

    public String getCountry() {
        return this.mapping.get(COUNTRY_TAG_ID);
    }

    public void setCountry(String country) {
        this.mapping.put(COUNTRY_TAG_ID, country);
    }

    public String getTag30() {
        return mapping.get(TAG30_TAG_ID);
    }

    public void setPayment(String payment) {
        tag30ForThaiQRCodeBillPayment.setValue(PAYMENTTYPE_TAG_ID, payment);
    }

    public void setBillProvider(String billProvider) {
        tag30ForThaiQRCodeBillPayment.setValue(BILLER_TAG_ID, billProvider);
    }

    public void setTicketNo(String ticketNo) {
        tag30ForThaiQRCodeBillPayment.setValue(TICKETNO_TAG_ID, ticketNo);
    }

    public String getPaymentID() {
        return tag30ForThaiQRCodeBillPayment.getPaymentID();
    }

    public String getBillerID() {
        return tag30ForThaiQRCodeBillPayment.getBillerID();
    }

    public String getTicketNo() {
        return tag30ForThaiQRCodeBillPayment.getTicketNo();
    }

    private String countLength(String input) {
        return StringUtils.leftPad("" + input.length(), 2, "0");
    }

    private String checkSum(String data) {

        int crc = 0xFFFF;
        int polynomial = 0x1021;
        byte[] bytes = data.getBytes();

        for (byte b : bytes) {
            for (int i = 0; i < 8; i++) {
                boolean bit = ((b >> (7 - i) & 1) == 1);
                boolean c15 = ((crc >> 15 & 1) == 1);
                crc <<= 1;
                if (c15 ^ bit) crc ^= polynomial;
            }
        }

        crc &= 0xffff;

        StringBuilder crcString = new StringBuilder(Integer.toHexString(crc));

        for (int index = 0; index < 4 - crcString.length(); index++) {
            crcString.insert(0, "0");
        }

        return crcString.toString();

    }

    public String getCheckSum() {
        return mapping.get(CHECKSUM_TAG_ID);
    }

    @Override
    public Schema parseSchema(String code) {
        String data = code.substring(0, code.length());
        //String checkSum = code.substring(code.length() - 8);

        while (!data.equals("")) {
            switch (data.substring(0, 2)) {
                case EMCO_TAG_ID:
                    data = putToHashMap(EMCO_TAG_ID, data);
                    break;

                case USAGETYPE_TAG_ID:
                    data = putToHashMap(USAGETYPE_TAG_ID, data);
                    break;

                case TAG30_TAG_ID:
                    data = putToHashMap(TAG30_TAG_ID, data);
                    tag30ParseSchema();
                    break;

                case COUNTRY_TAG_ID:
                    data = putToHashMap(COUNTRY_TAG_ID, data);
                    break;

                case REF_TAG_ID:
                    data = putToHashMap(REF_TAG_ID, data);
                    break;

                case CURRENCY_TAG_ID:
                    data = putToHashMap(CURRENCY_TAG_ID, data);
                    break;

                case AMOUNT_TAG_ID:
                    data = putToHashMap(AMOUNT_TAG_ID, data);
                    break;

                case CHECKSUM_TAG_ID:
                    data = putToHashMap(CHECKSUM_TAG_ID, data);
                    break;
            }
        }
        return null;
    }

    public String putToHashMap(String tagId, String data) {
        data = data.substring(2);
        int length = Integer.parseInt(data.substring(0, 2));
        data = data.substring(2);
        mapping.put(tagId, data.substring(0, length));
        data = data.substring(length);
        return data;
    }

    public void tag30ParseSchema() {
        mapping.put(TAG30_TAG_ID, "0016A0000006770101120115010552300935008020500129");
        String tag30String = mapping.get(TAG30_TAG_ID);
        while (!tag30String.equals("")) {
            tag30String = pushToTag30Class(tag30String.substring(0, 2), tag30String);
        }
    }

    private String pushToTag30Class(String tagID, String tag30String) {
        tag30String = tag30String.substring(2);
        int length = Integer.parseInt(tag30String.substring(0, 2));
        tag30String = tag30String.substring(2);
        tag30ForThaiQRCodeBillPayment.setValue(tagID, tag30String.substring(0, length));
        tag30String = tag30String.substring(length);
        return tag30String;
    }

    private boolean checkSumValidation(String data, String checkSumString) {
        if (checkSum(data).equals(checkSumString)) {
            return true;
        } else {
            return false;
        }
    }

    public String subString(String code) {
        //return code.substring(0, code.length() - 8);
        return code.substring(code.length() - 8, code.length());
    }

    @Override
    public String generateString() {

        mapping.put(TAG30_TAG_ID, tag30ForThaiQRCodeBillPayment.generateString());

        String data = EMCO_TAG_ID + countLength(this.mapping.get(EMCO_TAG_ID)) + getEmvcoQR() +
                USAGETYPE_TAG_ID + countLength(this.mapping.get(USAGETYPE_TAG_ID)) + getUseType() +
                TAG30_TAG_ID + getTag30() +
                COUNTRY_TAG_ID + countLength(this.mapping.get(COUNTRY_TAG_ID)) + getCountry() +
                REF_TAG_ID + ref +
                CURRENCY_TAG_ID + countLength(this.mapping.get(CURRENCY_TAG_ID)) + getCurrency() +
                AMOUNT_TAG_ID + countLength(this.mapping.get(AMOUNT_TAG_ID)) + getAmount();

        mapping.put(CHECKSUM_TAG_ID, checkSum(data));

        return data +
                CHECKSUM_TAG_ID +
                countLength(mapping.get(CHECKSUM_TAG_ID)) +
                mapping.get(CHECKSUM_TAG_ID);

    }
}
