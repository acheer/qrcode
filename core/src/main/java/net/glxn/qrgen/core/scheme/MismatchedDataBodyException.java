package net.glxn.qrgen.core.scheme;

public class MismatchedDataBodyException extends Exception{

    public MismatchedDataBodyException(){
        super();
    }

    public MismatchedDataBodyException(String message){
        super(message);
    }

}
