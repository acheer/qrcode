package net.glxn.qrgen.core.scheme;

import org.junit.Assert;
import org.junit.Test;

public class ThaiQRCodeBillPaymentTest {

    @Test
    public void testGenerateQRCode() {
        ThaiQRCodeBillPayment thaiQRCodeBillPayment = new ThaiQRCodeBillPayment();
        thaiQRCodeBillPayment.setEmvcoQR("01");
        thaiQRCodeBillPayment.setUseType("12");
        thaiQRCodeBillPayment.setCountry("TH");
        thaiQRCodeBillPayment.setCurrency("764");
        thaiQRCodeBillPayment.setAmount(400);
        thaiQRCodeBillPayment.setPayment("A000000677010112");
        thaiQRCodeBillPayment.setBillProvider("010552300935008");
        thaiQRCodeBillPayment.setTicketNo("00129");

        String expectedQRCode = "00020101021230480016A0000006770101120115010552300935008020500129" +
                "5802TH62200716SCOSM8001290999153037645406400.00640463f0";

        Assert.assertEquals(expectedQRCode, thaiQRCodeBillPayment.generateString());
    }

    @Test
    public void testSubString() {
        ThaiQRCodeBillPayment thaiQRCodeBillPayment = new ThaiQRCodeBillPayment();

        String expectedString = "640463f0";
        Assert.assertEquals(expectedString, thaiQRCodeBillPayment.subString("00020101021230480016A" +
                "0000006770101120115010552300935008020500129" +
                "5802TH62200716SCOSM8001290999153037645406400.00640463f0"));
    }

    @Test
    public void testParseSchemaEMCO() {
        ThaiQRCodeBillPayment thaiQRCodeBillPayment = new ThaiQRCodeBillPayment();
        thaiQRCodeBillPayment.parseSchema("00020101021230480016A" +
                "0000006770101120115010552300935008020500129" +
                "5802TH62200716SCOSM8001290999153037645406400.00640463f0");
        String expectedString = "01";
        Assert.assertEquals(expectedString, thaiQRCodeBillPayment.getEmvcoQR());
    }

    @Test
    public void testParseSchemaUSAGETYPE() {
        ThaiQRCodeBillPayment thaiQRCodeBillPayment = new ThaiQRCodeBillPayment();
        thaiQRCodeBillPayment.parseSchema("00020101021230480016A" +
                "0000006770101120115010552300935008020500129" +
                "5802TH62200716SCOSM8001290999153037645406400.00640463f0");
        String expectedString = "12";
        Assert.assertEquals(expectedString, thaiQRCodeBillPayment.getUseType());
    }

    @Test
    public void testParseSchemaCURRENCY() {
        ThaiQRCodeBillPayment thaiQRCodeBillPayment = new ThaiQRCodeBillPayment();
        thaiQRCodeBillPayment.parseSchema("00020101021230480016A" +
                "0000006770101120115010552300935008020500129" +
                "5802TH62200716SCOSM8001290999153037645406400.00640463f0");
        String expectedString = "764";
        Assert.assertEquals(expectedString, thaiQRCodeBillPayment.getCurrency());
    }

    @Test
    public void testParseSchemaAMOUNT() {
        ThaiQRCodeBillPayment thaiQRCodeBillPayment = new ThaiQRCodeBillPayment();
        thaiQRCodeBillPayment.parseSchema("00020101021230480016A" +
                "0000006770101120115010552300935008020500129" +
                "5802TH62200716SCOSM8001290999153037645406400.00640463f0");
        String expectedString = "400.00";
        Assert.assertEquals(expectedString, thaiQRCodeBillPayment.getAmount());
    }

    @Test
    public void testParseSchemaCOUNTRY() {
        ThaiQRCodeBillPayment thaiQRCodeBillPayment = new ThaiQRCodeBillPayment();
        thaiQRCodeBillPayment.parseSchema("00020101021230480016A" +
                "0000006770101120115010552300935008020500129" +
                "5802TH62200716SCOSM8001290999153037645406400.00640463f0");
        String expectedString = "TH";
        Assert.assertEquals(expectedString, thaiQRCodeBillPayment.getCountry());
    }

    @Test
    public void testParseSchemaCHECKSUM() {
        ThaiQRCodeBillPayment thaiQRCodeBillPayment = new ThaiQRCodeBillPayment();
        thaiQRCodeBillPayment.parseSchema("00020101021230480016A" +
                "0000006770101120115010552300935008020500129" +
                "5802TH62200716SCOSM8001290999153037645406400.00640463f0");
        String expectedString = "63f0";
        Assert.assertEquals(expectedString, thaiQRCodeBillPayment.getCheckSum());
    }

    @Test
    public void testPutToHashMapSingleCase() {
        ThaiQRCodeBillPayment thaiQRCodeBillPayment = new ThaiQRCodeBillPayment();
        String expectedString = "01021230480016A" +
                "0000006770101120115010552300935008020500129" +
                "5802TH62200716SCOSM8001290999153037645406400.00";
        Assert.assertEquals(expectedString, thaiQRCodeBillPayment.putToHashMap("00", "00020101021230480016A" +
                "0000006770101120115010552300935008020500129" +
                "5802TH62200716SCOSM8001290999153037645406400.00"));
    }

    @Test
    public void testTag30AsWholeTag() {
        ThaiQRCodeBillPayment thaiQRCodeBillPayment = new ThaiQRCodeBillPayment();
        thaiQRCodeBillPayment.parseSchema("00020101021230480016A" +
                "0000006770101120115010552300935008020500129" +
                "5802TH62200716SCOSM8001290999153037645406400.00640463f0");
        String expectedString = "0016A0000006770101120115010552300935008020500129";
        Assert.assertEquals(expectedString, thaiQRCodeBillPayment.getTag30());
    }

    @Test
    public void testStoreTag30inSeparatedDetail() {
        ThaiQRCodeBillPayment thaiQRCodeBillPayment = new ThaiQRCodeBillPayment();
        thaiQRCodeBillPayment.parseSchema("00020101021230480016A" +
                "0000006770101120115010552300935008020500129" +
                "5802TH62200716SCOSM8001290999153037645406400.00640463f0");
        String expectedPayment = "A000000677010112";
        String expectedBillerID = "010552300935008";
        String expectedTicketNo = "00129";

        Assert.assertEquals(expectedPayment, thaiQRCodeBillPayment.getPaymentID());
        Assert.assertEquals(expectedBillerID, thaiQRCodeBillPayment.getBillerID());
        Assert.assertEquals(expectedTicketNo, thaiQRCodeBillPayment.getTicketNo());

    }
}