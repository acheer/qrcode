package net.glxn.qrgen.core.scheme;

import org.junit.Assert;
import org.junit.Test;

public class Tag30ForThaiQRCodeBillPaymentTest {

    @Test
    public void generateString() {
        String expectedOutput = "480016A0000006770101120115010552300935008020500129";

        Tag30ForThaiQRCodeBillPayment tag30ForThaiQRCodeBillPayment = new Tag30ForThaiQRCodeBillPayment();

        tag30ForThaiQRCodeBillPayment.setValue("00","A000000677010112");
        tag30ForThaiQRCodeBillPayment.setValue("01","010552300935008");
        tag30ForThaiQRCodeBillPayment.setValue("02","00129");

        Assert.assertEquals(expectedOutput, tag30ForThaiQRCodeBillPayment.generateString());

    }
}